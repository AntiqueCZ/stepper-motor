# -*- coding: utf-8 -*-

from os import name
from time import sleep

# RPi importy
from gpiozero import DigitalOutputDevice

# nastavení ovládacích pinů
# white - +5V
ena = DigitalOutputDevice(13, active_high=False) # brown
sme = DigitalOutputDevice(19, active_high=True) # green
stp = DigitalOutputDevice(16, active_high=True) # yellow
inf = DigitalOutputDevice(12, active_high=True) # only LED info, no wire


class SteppingMotor():
    '''třída pro uložení stavových veličin ovládající krokový motor

        driver: TB6600
        krok: 1.8°
        plná otočka: 200 pulsů
        
        w[]   - rychlost otáčení hřídele (ot./min.)
        n[]   - přednastaený počet otáček na jeden click
        omega - aktuální zvolená rychlost otáčení
        direction - směr otáčení
        state - on/off
        pulse - počet pulsů na jednu otočku
        delay - prodleva před spuštěním motoru (čekání na refresh stránky)
    '''
    
    ver = "v1.2"
    
    def __init__(self):
        # přednastavené rychlosti (ot./min.)
        self.w = [0,0,0,0,0]
        self.n = [0,0,0,0,0]
        
        self.omega = 0
        self.direction = True
        self.state = False
        self.pulse = 200
        self.delay = 1.5
        
        # spuštěč pro počet otáček
        self.triggerN = 0
        self.triggerW = 0
        
        self.LoadSetup()
        self.SetOnOff(False)


    def LoadSetup(self):
        '''načtení parametrů ze souboru
        '''
        
        path = "config/motor.ini"
        
        with open(path,mode="r",encoding="utf-8") as f:
            
            for line in f:
                
                # rozdělím řádek na klíč a hodnotu, oddělovačem je dvojtečka s mezerou
                try:
                    key,val = line.split(': ')
                    val = val.strip()
                    
                # ignoruji nesmysly
                except:
                    continue
                    
                if key == "direction":
                    if val == "True":
                        self.direction = True
                    else:
                        self.direction = False
                        
                elif key == "omega":
                    try:
                        self.omega = int(val)
                    except:
                        print("V ini souboru je bota!!! - omega:  {}".format(self.omega))        
                elif key == "omega0":
                    try:
                        self.w[0] = int(val)
                    except:
                        print("V ini souboru je bota!!! - w[0]:  {}".format(self.w[0]))
                        
                elif key == "omega1":
                    try:
                        self.w[1] = int(val)
                    except:
                        print("V ini souboru je bota!!! - w[1]:  {}".format(self.w[1]))
                        
                elif key == "omega2":
                    try:
                        self.w[2] = int(val)
                    except:
                        print("V ini souboru je bota!!! - w[2]:  {}".format(self.w[2]))

                elif key == "omega3":
                    try:
                        self.w[3] = int(val)
                    except:
                        print("V ini souboru je bota!!! - w[3]:  {}".format(self.w[3]))
                        
                elif key == "omega4":
                    try:
                        self.w[4] = int(val)
                    except:
                        print("V ini souboru je bota!!! - w[4]:  {}".format(self.w[4]))
                        
                elif key == "otacky0":
                    try:
                        self.n[0] = int(val)
                    except:
                        print("V ini souboru je bota!!! - w[0]:  {}".format(self.n[0]))
                        
                elif key == "otacky1":
                    try:
                        self.n[1] = int(val)
                    except:
                        print("V ini souboru je bota!!! - w[1]:  {}".format(self.n[1]))
                        
                elif key == "otacky2":
                    try:
                        self.n[2] = int(val)
                    except:
                        print("V ini souboru je bota!!! - w[2]:  {}".format(self.n[2]))

                elif key == "otacky3":
                    try:
                        self.n[3] = int(val)
                    except:
                        print("V ini souboru je bota!!! - w[3]:  {}".format(self.n[3]))
                        
                elif key == "otacky4":
                    try:
                        self.n[4] = int(val)
                    except:
                        print("V ini souboru je bota!!! - w[4]:  {}".format(self.n[4]))
    
    
    def SaveSetup(self):
        """uložení parametrů do ini souboru
        """

        path = "config/motor.ini"
        
        with open(path,mode="w",encoding="utf-8") as f:
            
            f.write("# parametry pro řízení krokového motoru\n")
            
            if self.direction == True:
                f.write("direction: True\n")
            else:
                f.write("direction: False\n")
                
            f.write("omega: {}\n".format(self.omega))

            for i in range(5):
                f.write("omega{}: {}\n".format(i,self.w[i]))
                
            for i in range(5):
                f.write("otacky{}: {}\n".format(i,self.n[i]))    


    def wToTime(self):
        """přepočet rychlost otáčení v ot./min.

        Returns:
            float: 1/2 periody v sec.
        """
        
        period = 1 / ((self.omega / 60) * self.pulse)
        
        return (period / 4)
        

    def Run(self,n=0):
        """otočí n krát dokola hřídelí motoru

        Args:
            n (int, optional): počet plných otoček hřídele. Defaults to 0.
        """
        
        if n == 0:
            stp.off()
            return
        
        print("Čekám...")
        sleep(self.delay)
        print("Spouštím točení N={} otáček hřídele rychlostí ω={} ot./min.".format(n,self.omega))
        
        t = self.wToTime()
        print("t = {} sec.".format(t))
        
        # pak spustím blikání výstupu
        if n == -1:
            stp.blink(t,t,None,False)
        else:
            # zjistím celkový počet pulsů
            N = n * self.pulse
            stp.blink(t,t,N,False)
        
        return


    def Stop(self):
        """zastaví otáčení motoru
        """
        print("Zastavuji motor...")
        stp.off()
        
        return


    def SetDirection(self,d=False):
        """nastaví směr otáčení hřídele

        Args:
            d (bool, optional): směr otáčení hřídele. Defaults to False.
        """
        
        # nastavuji aktuální stav do proměnné kvůli případnému zápisu do ini
        self.direction = d
        
        if d != sme.value:
            sme.toggle()
             
        return


    def SetOnOff(self,d=False):
        """zapne vypne otáčení motoru (enable na driveru)

        Args:
            d (bool, optional): Zapíná/vypíná otáčení motoru. Defaults to False.
        """
        
        self.state = d
        
        if d != ena.value:
            ena.toggle()
            
        return


    def SetW(self,w=0):
        """nastaví aktuální rychlost otáčení
        
        MAX limituji na 2000 ot./min.

        Args:
            w (int, optional): Rychlost otáčení hřídele v ot./min.. Defaults to 0.
            
        """
        
        if w > 2000:
            w = 2000
        
        self.omega = w
        
        return
    

if __name__ == "__main__":
    
    motor = SteppingMotor()
    
    print("Směr otáčení motoru: {}".format(motor.direction))
    
    for i in range(5):
        print("Rychlost omega{} = {} ot./min.".format(i,motor.w[i]))
        
    for i in range(5):
        print("Počet otáček{} = {} ".format(i,motor.n[i]))
        
    motor.w[4] += 100
    motor.w[3] += 100
    motor.w[2] += 100
    motor.w[1] += 100
    motor.w[0] += 100
    
    motor.n[4] += 10
    motor.n[3] += 10
    motor.n[2] += 10
    motor.n[1] += 10
    motor.n[0] += 10
      
    motor.SaveSetup()
    