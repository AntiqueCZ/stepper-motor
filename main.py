# -*- coding: utf-8 -*-


# importy knihoven
from flask import Flask, render_template, request
from time import sleep
from datetime import datetime
from os import system

import subprocess
import threading

# RPi importy
from motor import SteppingMotor
from wifi import wifitool

motor = SteppingMotor()
    
def motor_thread():
    
    # Na úvod si zamrkám infoLEDkou, ať je k něčemu dobrá
    # delay = 0.060
    # inf.blink(delay,delay,5,False)
    
    print("Startuji aplikaci...")
        
    
    # nekonečná smyčka...
    while 1:
        if motor.omega == 0:
            continue
        
        if motor.state == False:
            continue
        
        if motor.triggerN:
           motor.Run(motor.triggerN)
           motor.triggerN = 0
        
        if motor.triggerW:
            motor.Run(-1)
            motor.triggerW = 0


# zapouzdření instance třídy Flask do funkce 
# pro spuštění ve vlákně na pozadí
def threadWeb():
    WebServer = Flask(__name__)
   
    # stránka indexu
    @WebServer.route("/", methods = ["GET", "POST"])
    def index():
        
        motor_form = request.form
        
        # test příjmu dat z formuláře
        if len(motor_form):
            
            if motor_form["setup"] == "True":
                # data ze stránky setup
                # ---------------------------------------------------
                # nastavení rychlostí
                for i in range(5):
                    try:
                        motor.w[i] = int(motor_form["w{}".format(i)])
                    except:
                        print("Chybně zadaná hodnota pro parametr motor.w[{}] = {}".
                              format(i,motor_form["w{}".format(i)]))
                        print("Hodnotu ponechávám nezměněnou…")
                    try:
                        motor.n[i] = int(motor_form["n{}".format(i)])
                    except:
                        print("Chybně zadaná hodnota pro parametr motor.n[{}] = {}".
                              format(i,motor_form["n{}".format(i)]))
                        print("Hodnotu ponechávám nezměněnou…")
                        
                    motor.SaveSetup()
                    
            
            else:
                # data ze stránky bypass
                # --------------------------------
                # řízení směru
                if motor_form["motor"] == "True":
                    motor.SetDirection(True)
                    
                elif motor_form["motor"] == "False":
                    motor.SetDirection(False)
                    
                    
                # vypínání/zapínání motoru
                elif motor_form["motor"] == "ON":
                    motor.SetOnOff(True)
                    
                elif motor_form["motor"] == "OFF":
                    motor.SetOnOff(False)
                    
                    
                # nastavení rychlosti otáčení
                elif motor_form["motor"] == "w0":
                    motor.SetW(motor.w[0])
                    print("Nastavuji trigger na {}".format(motor.w[0]))
                    motor.triggerW = 1

                    
                elif motor_form["motor"] == "w1":
                    motor.SetW(motor.w[1])                    
                    print("Nastavuji trigger na {}".format(motor.w[1]))
                    motor.triggerW = 1

                    
                elif motor_form["motor"] == "w2":
                    motor.SetW(motor.w[2])
                    print("Nastavuji trigger na {}".format(motor.w[2]))
                    motor.triggerW = 1

                    
                elif motor_form["motor"] == "w3":
                    motor.SetW(motor.w[3])
                    print("Nastavuji trigger na {}".format(motor.w[3]))
                    motor.triggerW = 1

                    
                elif motor_form["motor"] == "w4":
                    motor.SetW(motor.w[4])
                    print("Nastavuji trigger na {}".format(motor.w[4]))
                    motor.triggerW = 1

                    
                elif motor_form["motor"] == "run":
                    try:
                        motor.omega = int(motor_form["wx"])
                    except:
                        print("Zadaná hodnota ω={} ot./min. je mimo povolený rozsah...".format(motor_form["wx"]))
                        
                    print("Nastavuji trigger na {}".format(motor.omega))
                    motor.triggerW = 1
                    
                # zastavení motoru    
                elif motor_form["motor"] == "stop":
                    motor.triggerN = 0
                    motor.triggerW = 0
                    motor.Stop()
                    
                # počet otoček
                elif motor_form["motor"] == "n0":
                    print("Nastavuji trigger na {}".format(motor.n[0]))
                    motor.triggerN = motor.n[0]
                    
                elif motor_form["motor"] == "n1":
                    print("Nastavuji trigger na {}".format(motor.n[1]))
                    motor.triggerN = motor.n[1]
                    
                elif motor_form["motor"] == "n2":
                    print("Nastavuji trigger na {}".format(motor.n[2]))
                    motor.triggerN = motor.n[2]
                    
                elif motor_form["motor"] == "n3":
                    print("Nastavuji trigger na {}".format(motor.n[3]))
                    motor.triggerN = motor.n[3]
                    
                elif motor_form["motor"] == "n4":
                    print("Nastavuji trigger na {}".format(motor.n[4]))
                    motor.triggerN = motor.n[4]

                # slepá větev 
                else:
                    print("Nespecifikovaná chyba, sem bych se vůbec neměl dostat...")
                    
                        
        return render_template("index.html",motor=motor)
 
 
    # stránka nastavení rychlostí
    @WebServer.route("/setup", methods = ["GET", "POST"])
    def setup():
        
        # motor_form = request.form
        
        return render_template("setup.html",motor=motor)


    # stránka pro upgrade
    @WebServer.route("/upg", methods = ["GET", "POST"])
    def upg():
        
        motor_form = request.form
        
        # test příjmu dat z formuláře
        if len(motor_form):
            
            if motor_form['RPi'] == 'True':
                print("Stahuji nové kódy z GitLabu...")
                subprocess.run(['sh', '/home/pi/stepper-motor/git_pull.sh'])
            
            
        return render_template("upg.html",motor=motor)

 
    # spuštění web serveru
    WebServer.run(debug=True, use_reloader=False, host="0.0.0.0", port="5000")
    

# ---------------------------------
# spuštění html serveru
# ---------------------------------

# spuštění instance třídy Flask / aplikace
if __name__ == "__main__":
    
         
    w = wifitool()
    w.CheckFile()
        
    # threadWeb()
    t_WebApp = threading.Thread(name='Flask Driving Stepping Motor', target=threadWeb)
    t_WebApp.setDaemon(True)
    t_WebApp.start()
    
    try:
        motor_thread()
        # threadWeb()

    except KeyboardInterrupt:
        print("končím...")
        exit(0)
    
    
    