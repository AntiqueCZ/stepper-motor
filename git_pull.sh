#!/bin/sh
# Skript pro aktualizaci SW v Raspberry Pi
# nejprve je nutno
# vypnout běžící programy
pkill python3


# čidla mají své oddělené procesy
# for pid in `ps -ef | grep adafruit | awk '{print $2}'` ;
#    do kill $pid ;
#    echo $pid ;
# done

# pak už je možné stáhnout z GitLabu nové/aktualizované soubory
cd /home/pi/stepper-motor
git pull

sudo reboot
