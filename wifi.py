# -*- coding: utf-8 -*-

from pathlib import Path
import os

class wifitool():
    """třída pro definici funkce, kontrolojící existenci soubor k nastavení připojení k wifi
    """
    
    def __init__(self):
        
        self.path = Path("/boot/wpa_supplicant.conf.bak")
    
    def CheckFile(self):
        """test existence souboru
        """
        
        if not self.path.is_file():
            try:
                self.CreateFile()
                
            except:
                print("nepodařilo se vytvořit záložní soubor pro WiFi")
                
        else:
            print("soubor pro nastvení WiFi již existuje…")
                
                
        
        
    
    def CreateFile(self):
        """vytvoření souboru
        """
        
        cmd = "sudo touch {}".format(self.path)
        
        os.system(cmd)
        
        uid = int(os.environ.get('SUDO_UID'))
        gid = int(os.environ.get('SUDO_GID'))

        os.chown(self.path, uid, gid)
        
        with open(self.path,'w') as f:
            
            f.write('ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n')
            f.write('update_config=1\n')
            f.write('country=CZ\n')
            f.write('network={\n')
            f.write('\tssid="jmeno wifi sítě"\n')
            f.write('\tpsk="heslo wifi"\n')
            f.write('\tkey_mgmt=WPA-PSK\n')
            f.write('}\n')
            
        print("soubor pro WiFi nastavení vytvořen…")
